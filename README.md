# Hello, I'm Dave :wave:

I appreciate you reading this. My name is Dave Langemak (pronounced LAYNG-mak) and I'm a Director, Strategy and Operations on the Chief of Staff Team to the CEO (COST.) This README document is intended to help you get to know me and understand how best to work with me.

## My history
* I grew up in Cupertino, California, USA and earned my undergraduate degree in Electrical Engineering and Computer Science from the University of California, Berkeley.
* I pursued a Masters in Organization Development from Pepperdine University, completing my studies in August 2022. It is an amazing program, ask me if you want to learn more about it.
* I reside with my wife, Kathryn, and two children, Olivia and Amelia. I am dedicated to my family, and relish spending time with them - from walking my children to school every morning to embarking on a 23-day journey to Japan. 
* I am an enthusiast of science fiction and fantasy, and I'm always on the lookout for engaging TV shows, movies, and book recommendations.
* I've been investing throughout my life, and recently I've been learning about commercial real estate.
* In my leisure time, I enjoy cooperative and competitive board games, computer and mobile games.

## Career Highlights
* I started my career as a software developer and pivoted to be the co-founder of a small Internet Systems Engineering company. Most recently I held a series of technical program management (TPM) roles at Google, including leading named Android Platform releases (Lollipop & Marshmallow), building and managing a TPM team, and as Chief of Staff for a core Android engineering team. I'm excited to continue my journey at GitLab!

## About me
* I get up early and go to bed early. I have a natural aversion to caffeine (I bring water or tea to coffee chats!)
* I wake up full of energy and I usually dedicate the first few hours of my day to focus concentration. My energy wanes in the afternoon and evening.
* I assume everyone is doing the best they can. I sincerely want to know how you're doing.
* I struggle to remember things, especially when learning new things (for example meeting someone for the first time.) If I forget something you share with me, don't take it personally. During virtual chats I often take handwritten notes as a memory aid.
* I enjoy asking questions and telling stories. When I'm figuring something out, it helps me to verbally process. If I'm doing any of these and you want me to stop, please interrupt me and say something.

## How you can help me
* I'm a lifelong learner and I appreciate the gift of feedback. Please reach out if there's an opportunity for me to learn and grow. I'm happy to process with you if you're unsure how to put it in words.

## My working style
* I'm adapting to GitLab's culture. For now, I'm building habits to embrace Slack.
* I review my calendar regularly. I respond to recurring meetings when I do a review, as it helps me frame my week.

## Strengths/Weaknesses
* I naturally reframe situations in an affirmative way (e.g. what's the opportunity here?)
* I've been told I'm very inclusive in my work. I enjoy building systems & processes that enable everyone to contribute.
* I can be detail oriented in my work. If I'm working with someone with a similar style this can lead to endless word smithing.
* I've had to learn to pause and celebrate successes or endings - my tendency is to immediately look to the next challenge. I'm developing here, and appreciate being reminded to consider what was accomplished.
* I can overthink decisions. I'm appreciating the Iteration value!
